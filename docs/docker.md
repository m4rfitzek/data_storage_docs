# Docker installation

## Prerequisites

1. Install [docker](https://docs.docker.com/get-docker/)

## Building Image

    docker build -t <Tag name> .

    docker build -t iot_data_storage .

## Run

    docker run -d -p <host_port>:3000 <Tag name>

    docker run -d -p 3000:3000 iot_data_storage

## Stop container

find container id of your container

    docker container ls

stop container

    docker container stop <CONTAINER ID>