# Welcome

This is documentation for my project [IoT Data Storage](https://gitlab.com/m4rfitzek/iot_data_storage_server)

## About

This system is designed to managing your IoT devices, storing device data and exporting stored data.

You can set MQTT or HTTP protocol for communication between the device and the server.

Data can be exported to CSV, JSON and XML formats.
