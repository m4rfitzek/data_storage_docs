#!/bin/bash

VER=$(git log --pretty=format:'/ ver. %h / %ai' -n 1)
sed "s,%VER%, $VER," mkdocs.yml -i
YEAR=$(date +"%Y")
sed "s,%YEAR%, $YEAR," mkdocs.yml -i
